# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  dict = {}
  str.split.each { |word| dict[word] = word.length }
  dict
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_k, v| v }[-1][0]

end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count_hash = Hash.new(0)
  word.each_char { |char| count_hash[char] += 1 }
  count_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hsh = {}
  arr.each { |el| hsh[el] = 1 unless hsh.include?(el) }
  hsh.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hsh = { even:0, odd:0 }
  numbers.each do |el|
    if el.even?
      hsh[:even] += 1
    else
      hsh[:odd] += 1
    end
  end
  hsh
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hsh = { 'a' => 0, 'e' => 0, 'i' => 0, 'o' => 0, 'u' => 0 }
  string.each_char do |let|
    hsh[let.downcase] += 1 if hsh.include?(let.downcase)
  end
  sort_hash = hsh.sort_by { |_k, v| v }
  acc = ["", 0]
  sort_hash.each do |el|
    if el[1] > acc[1]
      acc = el
    elsif el[1] == acc[1]
      acc = el if el[0] < acc[0]
    end
  end
  acc[0]
end


# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half_only(students).combination(2)
end

def second_half_only(students)
  second_half = []
  students.each { |k, v| second_half.push(k) if v > 6 }
  second_half
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  # note on hash: hsh = {"species" => population_size}
  sorted_pops = pop_sort(pop_count(specimens))
  biodiversity_eq(sorted_pops)
end

def pop_count(specimens)
  populations = Hash.new(0)
  specimens.each { |el| populations[el] += 1 }
  populations
end

def pop_sort(hsh)
  hsh.sort_by { |_k, v| v }
end

def biodiversity_eq(pop)
  # number of species**2
  (pop.length**2) * (pop[0][-1] / pop[-1][-1])
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  wortschatz = character_count(string_cleaner(normal_sign))
  possible = character_count(string_cleaner(vandalized_sign))
  possible.each do |k, v|
    return false if !wortschatz.key?(k)
    return false if v > wortschatz[k]
  end
  true
end

def character_count(str)
  hsh = Hash.new(0)
  str.each_char do |let|
    next if let == " "
    hsh[let] += 1
  end
  hsh
end

def string_cleaner(str)
  str.downcase.delete(",.?;:!-")
end
